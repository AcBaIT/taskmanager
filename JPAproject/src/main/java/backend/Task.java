	package backend;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "jpaStarterTask")
public class Task implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID", unique = true)
	private int id;
	
	@Column(name = "Title", nullable = false)
	private String title;
	
	@Column(name = "Timestamp")
	private Timestamp timestamp;
	
	@Column(name = "Comment", nullable = true)
	private String comment;
	
	@Column(name = "Status", nullable = false)
	private boolean status = false;
	
	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean isStatus() {
		return status;
	}

}
