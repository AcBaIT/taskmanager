package backend;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@NamedQueries({ @NamedQuery(name = "gettaskByName", query = "SELECT t FROM Task t where t.title= :name"),
		@NamedQuery(name = "getAllTasks", query = "SELECT t FROM Task t"),
		@NamedQuery(name = "deleteTaskById", query = "DELETE t from Task t where t.id = :id"),
		@NamedQuery(name = "updateTaskById", query = "") })
@SpringBootApplication
public class Main {
	private static EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("backend");

	public static void main(String[] args) {
//		Fill test data to DB
//		addTask("Fix project A", "There is a mistake from which the project doesn't start in main class");
//		addTask("Groceries", "Need food asap");
//		addTask("Work on JPA project", "Need to get the JPA project done and maybe even add a front end");
//		getTaskByName("Groceries");
//		getTasks();

		ENTITY_MANAGER_FACTORY.close();
		SpringApplication.run(Task.class, args);

	}

	public static void addTask(String title, String comment) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
		try {
			et = em.getTransaction();
			et.begin();
			Task task = new Task();
			task.setComment(comment);
			task.setTitle(title);
			em.persist(task);
			et.commit();
		} catch (Exception e) {
			if (et != null) {
				et.rollback();
			}
			e.printStackTrace();
		} finally {
			em.close();
		}
	}

	public static void updateTask() {

	}

	public static List<Task> getTaskByName(String name) {
		System.out.println("\n\n" + name + " Task \n__________________\n");
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		Query query = em.createQuery("getTaskByName", Task.class).setParameter("name", name);

		@SuppressWarnings("unchecked")
		List<Task> list = query.getResultList();
		return list;
	}

	public static List<Task> getTasks() {
		System.out.println("\n\nTask List \n__________________\n");
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		Query query = em.createQuery("getAllTasks", Task.class);
		// Returns how exactly the database received our Query
		String taskQuery = query.unwrap(org.hibernate.query.internal.QueryImpl.class).getQueryString();
		System.out.println(taskQuery);

		@SuppressWarnings("unchecked")
		List<Task> list = query.getResultList();
		System.out.println(list);
		return list;
	}

	public static Task getTaskById() {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		Task task = em.find(Task.class, 1);
		return task;
	}

	public static void deleteTask() {

	}

}
